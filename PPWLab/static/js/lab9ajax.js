$.ajax({
	url: "{% url 'books_data' %}",
	datatype: 'json',
	success: function(result){
		var obj = jQuery.parseJSON(result)
		console.log(obj);
		renderHTML(obj);
	}
})

var container = document.getElementById("demo");
function renderHTML(data){
	htmlstring = "<tbody>";
	for(i = 0; i < data.items.length;i++){
		htmlstring += "<tr>"+
        "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
        "<td><img class='img-fluid' style='width:25vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
        "<td class='lign-middle'>" + data.items[i].volumeInfo.title +"</td>" +
		"<td class='lign-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
        "<td class='lign-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
        "<td class='lign-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
		"<td style=" + "'text-align:center'>" + "<img width='28px' src='{% static 'img/star2.png' %}'>" + "</td></tr>";
	} 
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
}

var counter = 0;
function favorite(clicked_id){
	var bintang = document.getElementById(clicked_id);
	if(bintang.classList.contains("checked")){
        bintang.classList.remove("checked");
        bintang.attr("{% static 'img/star.png' %}", "{% static 'img/star2.png' %}")
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
    }
	else{
        bintang.classList.add('checked');
        bintang.attr("{% static 'img/star2.png' %}", "{% static 'img/star.png' %}")
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
    }
}

