$("document").ready(function() {
    $("input").change(function() {
      if(this.checked){
          $('link[href="/static/css/lab8-light.css"]').attr('href', '/static/css/lab8-dark.css');
          document.getElementById('night').innerHTML = '<strong>Good Night!</strong>';
        } else {
          $('link[href="/static/css/lab8-dark.css"]').attr('href', '/static/css/lab8-light.css');
          document.getElementById('night').innerHTML = '<strong>Night Mode?</strong>';
        }
    });
    
    $(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
        });
    });
})

setTimeout(function() {
    setTimeout(function() {
        $('.pageLoad').addClass('off');
    }, 0)
    setTimeout(function(){
        $("#page").css({"visibility":"visible"});
    },300)
}, 1500)
