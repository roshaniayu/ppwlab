from django.test import TestCase
from django.test import Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import create_form, profilegue
from .models import Kabar
from .forms import FormKabar
import unittest
import time

# Create your tests here.
class Lab6_Test(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_to_do_list_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'howru.html')

    def test_lab6_using_create_form_func(self):
        found= resolve('/lab6/')
        self.assertEqual(found.func, create_form)

    def test_status_created(self):
        contoh_mood =  "capek"
        early_count = Kabar.objects.count()
        status_test = Kabar.objects.create(mood=contoh_mood)
        last_count = Kabar.objects.count()
        self.assertEqual(early_count + 1, last_count)  
        self.assertEqual(status_test.mood, contoh_mood)

class Lab6_Profile_Test(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab6/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_to_do_list_template(self):
        response = Client().get('/lab6/profile/')
        self.assertTemplateUsed(response, 'profilegue.html')

    def test_lab6_using_create_form_func(self):
        found= resolve('/lab6/profile/')
        self.assertEqual(found.func, profilegue)

class Lab6FunctionalTest(TestCase):
    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=op) 
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab6/')
        time.sleep(2)
        description = browser.find_element_by_id('id_mood')
        submit = browser.find_element_by_id('submit')
        description.send_keys('coba-coba')
        time.sleep(2)
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('coba-coba', browser.page_source)

    def test_layout_page_title(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab6/')
        self.assertIn('how r u?', self.browser.title)
        time.sleep(2)

    def test_layout_header(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab6/')
        header = browser.find_element_by_id('id_title')
        header_text = self.browser.find_element_by_tag_name('p').text
        self.assertIn('Hello! How are you?', header_text)
        time.sleep(2)

    def test_layout_header_with_css_property(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab6/')
        content = browser.find_element_by_tag_name('p').value_of_css_property('text-align')
        self.assertIn('center', content)
        time.sleep(2)

    def test_layout_response_header_with_css_property(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab6/')
        content = browser.find_element_by_tag_name('p').value_of_css_property('color')
        self.assertIn('rgba(255, 255, 255, 1)', content)
        time.sleep(2)
