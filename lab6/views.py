from django.shortcuts import render, redirect
from .forms import FormKabar
from .models import Kabar

# Create your views here.
def create_form(request):
    form = FormKabar(request.POST or None)
    response = {}
    if(request.method == "POST" and form.is_valid()):
        mood = request.POST.get("mood")
        Kabar.objects.create(mood=mood)
        return redirect('howru')

    kabar2 = Kabar.objects.all().order_by("-id")
    response = {
        "kabar2" : kabar2,
        "form" : form,
    }
    return render(request, 'howru.html', response)

def profilegue(request):
    response = {}
    return render(request,'profilegue.html',response)