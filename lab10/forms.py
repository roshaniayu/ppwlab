from django import forms

class FormSubscriber(forms.Form):
    name = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Name', 'id' : 'name'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, max_length=12, widget=forms.TextInput(attrs={'type': 'password', 'label': 'Password', 'id' : 'password'}))
