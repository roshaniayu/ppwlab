from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import indexlab11, index, booklist
import unittest
import time

# Create your tests here.
class Lab11_Test(TestCase):
    def test_lab_11_url_is_exist(self):
        response = Client().get('/lab11/')
        self.assertEqual(response.status_code, 200)

    def test_lab_11_booklist_is_exist(self):
        response = Client().get('/lab11/book-list/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab11/book-list/json/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab11/book-list/like/')
        self.assertEqual(response.status_code, 200)
    
    def test_json_data_url_exists(self):
        response = Client().get('/lab11/book-list/unlike/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab11/book-list/get-like/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab11_using_signin_template(self):
        response = Client().get('/lab11/')
        self.assertTemplateUsed(response, 'signin.html')
    
    def test_lab11_using_booklist_template(self):
        response = Client().get('/lab11/book-list/')
        self.assertTemplateUsed(response, 'booklist.html')

    def test_lab11_using_indexlab11_func(self):
        found = resolve('/lab11/')
        self.assertEqual(found.func, indexlab11)

    def test_lab11_using_index_func(self):
        found = resolve('/lab11/book-list/')
        self.assertEqual(found.func, index)

    def test_lab11_using_booklist_func(self):
        found = resolve('/lab11/book-list/json/')
        self.assertEqual(found.func, booklist)

#     def test_favorite_logged_in(self):
#         c = self.client.session
#         c['user_id']='test'
#         c['name']='asti'
#         c.save()
#         response = Client().get('/favorite/')
#         self.assertEqual(response.status_code, 302)

#     def test_logged_in_page_is_exist(self):
#         response = Client().get('/favorite/')
#         self.assertEqual(response.status_code, 302)

#     def test_using_index_func(self):
#         found = resolve('/favorite/')
#         self.assertEqual(found.func, favorite)

#     def test_client_can_POST_and_render(self):
#         s = self.client.session
#         s['user_id'] = 'test'
#         s['name'] = 'asti'
#         s.save()
#         response_post = self.client.post('/', {'id': 'abcd'})
#         self.assertEqual(response_post.status_code, 200)

#     def test_client_can_delete_and_delete_selected(self):
#         title = "hehehe"
#         s = self.client.session
#         s['user_id'] = 'test'
#         s['name'] = 'asti'
#         s['book'] = [title]
#         s.save()
#         response_post = self.client.post('/favorite/cekFav?q=quiliting', {'id': title})
#         self.assertEqual(response_post.status_code, 200)

#     def test_client_can_get_data(self):
#         s = self.client.session
#         s['user_id'] = 'test'
#         s['name'] = 'asti'
#         s['book'] = []
#         s.save()
#         response = self.client.get('/favorite/cekFav?q=quiliting')
#         self.assertEqual(response.status_code, 200)

#     def test_books_not_favorited(self):
#         s = self.client.session
#         s['user_id'] = 'test'
#         s['name'] = 'asti'
#         s['book'] = []
#         s.save()
#         response = self.client.get('/favorite/cekFav?q=quiliting').json()
#         bool = response['data'][0]['boolean']
#         self.assertFalse(bool)
