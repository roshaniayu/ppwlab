from django.shortcuts import render, redirect
from .forms import FormAgenda
from .models import Agenda

# Create your views here.
def agenda(request):
    response = {}
    agendas = Agenda.objects.all()
    response = {
        "agendas" : agendas
    }
    return render(request, 'agenda.html', response)

def create_agenda(request):
    form = FormAgenda(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            activity = request.POST.get("activity")
            day = request.POST.get("day")
            date = request.POST.get("date")
            time = request.POST.get("time")
            place = request.POST.get("place")
            category = request.POST.get("category")
            Agenda.objects.create(activity=activity, day=day, date=date, time=time, place=place, category=category)
            return redirect('agenda')
        else:
            return render(request, 'createagenda.html', response)
    else:
        response['form'] = form
        return render(request, 'createagenda.html', response)

def delete_agenda(request):
    Agenda.objects.filter().delete()
    return redirect('agenda')