from django import forms

class FormAgenda(forms.Form):
    OPTION = (
        ('Academic', 'Academic'),
        ('Non Academic', 'Non Academic'),
        ('Others', 'Others'),
    )
    activity = forms.CharField(label="Activity")
    day = forms.CharField(label="Day")
    date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
    place = forms.CharField(label="Place")
    category = forms.ChoiceField(label="Category", choices=OPTION)